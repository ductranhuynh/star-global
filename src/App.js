import React, { useState, useEffect } from 'react';
import './App.scss';
import { BOX_WIDTH, BOX_HEIGHT } from './constants';

function App() {
  const ww = window.innerWidth;
  const wh = window.innerHeight;
  const [transitionSpeed, setTransitionSpeed] = useState(1000)
  const [{x, y}, setPosition] = useState({x: 0, y: 0});

  const handleClick = ({ x, y}) => {
    if (x + BOX_WIDTH > ww) {
      x = ww - BOX_WIDTH;
    }

    if (y + BOX_HEIGHT > wh) {
      y = wh - BOX_HEIGHT;
    }
    setPosition({
      x,
      y
    });
  };

  const handleInputChange = e => {
    const { value } = e.target;
    if (Number.isInteger(+value)) {
     setTransitionSpeed(e.target.value);
    }
  };

  const handleReset = (e) => {
    e.stopPropagation();
    setTransitionSpeed(1000);
    setPosition({x: 0, y: 0});
  };

  useEffect(() => {
    document.addEventListener('click', handleClick)
    return () => {
      document.removeEventListener('click', handleClick);
    }
  });

  return (
    <div className="app">
      <div className="container">
        <div className="form-group">
          <div className="form-label">Transition speed</div>
          <input
            type="text"
            className="form-input"
            value={transitionSpeed}
            onChange={handleInputChange}
            onClick={e => e.stopPropagation()}
          />
          <button
            type="button"
            className="btn"
            onClick={handleReset}
          >
            Reset
          </button>
        </div>
        <p>Position ({x},{y}) - Transition speed: {transitionSpeed / 1000}s</p>
        <p>Click any point on the screen to move the box</p>
      </div>
      <div
        className="box"
        style={{
          transform: `translate(${x}px, ${y}px)`,
          transition: `transform ease-out ${transitionSpeed / 1000}s`
        }}
      />
    </div>
  );
}

export default App;
